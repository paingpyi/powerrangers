//
//  main.m
//  PowerRangers
//
//  Created by Paing Pyi on 14/1/14.
//  Copyright (c) 2014 Paing. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

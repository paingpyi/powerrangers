//
//  PowerRangerView.m
//  PowerRangers
//
//  Created by Paing Pyi on 15/1/14.
//  Copyright (c) 2014 Paing. All rights reserved.
//

#import "PowerRangerView.h"

@interface PowerRangerView()

@property BOOL shouldAnimate;

@end

@implementation PowerRangerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame andObject:(PowerRanger*)pr
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = pr.getUIColor;
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
}
//*/

- (void)startAnimate
{
    [UIView animateWithDuration:0.3 animations:^{

         CGAffineTransform rotateTrans =
         CGAffineTransformMakeRotation(90 * M_PI / 180);
        self.transform = rotateTrans;
        
    } completion:nil];
    

}
- (void)stopAnimate
{
    [UIView animateWithDuration:0.4 animations:^{
        
        CGAffineTransform rotateTrans =
        CGAffineTransformMakeRotation(0 * M_PI / 180);
        self.transform = rotateTrans;
        
    } completion:nil];
}

@end

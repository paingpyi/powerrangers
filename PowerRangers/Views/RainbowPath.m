//
//  PowerRangerView.m
//  PowerRangers
//
//  Created by Paing Pyi on 15/1/14.
//  Copyright (c) 2014 Paing. All rights reserved.
//

#import "RainbowPath.h"

@implementation RainbowPath

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor redColor];
        
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame withColor:(UIColor*)color
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = color;
    }
    return self;
}


- (void)rainbowPath
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *gradientColor = [UIColor colorWithRed:0.51 green:0.0 blue:0.49 alpha:1.0];
    
    NSArray *gradientColors = [NSArray arrayWithObjects:
                               (id)[UIColor blueColor].CGColor,
                              (id)[UIColor yellowColor].CGColor,
                               (id)gradientColor.CGColor,
                               (id)[UIColor greenColor].CGColor,
                               (id)[UIColor redColor].CGColor, nil];
    CGFloat gradientLocations[] = {0, 0.3, 0.5, 0.8, 1};
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)gradientColors, gradientLocations);

//    CAGradientLayer *maskLayer = [CAGradientLayer layer];
    

//    UIBezierPath *roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.frame.size.height, self.frame.size.width) cornerRadius:0];
    UIBezierPath *roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) cornerRadius:0];
    CGContextSaveGState(context);
    [roundedRectanglePath fill];
    [roundedRectanglePath addClip];
//    CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, 50), 0);
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0.0, 0.5), CGPointMake(1.0, 0.5), 0);
//    [gradient setStartPoint:CGPointMake(0.0, 0.5)];
//    [gradient setEndPoint:CGPointMake(1.0, 0.5)];

    
    CGColorSpaceRelease(colorSpace);
    CGGradientRelease(gradient);

}



@end

//
//  PowerRangerView.h
//  PowerRangers
//
//  Created by Paing Pyi on 15/1/14.
//  Copyright (c) 2014 Paing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RainbowPath : UIView
- (id)initWithFrame:(CGRect)frame withColor:(UIColor*)color;
@end

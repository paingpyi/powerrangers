#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *viewImg;
@property (strong, nonatomic) IBOutlet UILabel *label;

- (void)configureWithData:(id)modelUnknown;
+ (CGFloat)getHeight;
@end

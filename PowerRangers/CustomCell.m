#import "CustomCell.h"

@interface CustomCell()
@property (nonatomic, strong) PowerRanger *pr;
@end

@implementation CustomCell

- (void)configureWithData:(id)modelUnknown
{
    self.pr = (PowerRanger*)modelUnknown;
    
    UIColor *color;
    
    if(self.pr.activeValue){
        color = [UIColor grayColor];
    }else{
        color = [self.pr getUIColor];
    }

    // name
    self.label.text = [NSString stringWithFormat:@"%@",self.pr.name];
    self.label.textColor = color;
    
    // color
    self.viewImg.backgroundColor = color;
}

+ (CGFloat)getHeight
{
    UITableViewCell *cell;
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self
                                                 options:nil];
    if(array.count > 0)
        cell = [array objectAtIndex:0];
    
    CGFloat cellHeight = cell.bounds.size.height;
    return cellHeight;
}

@end

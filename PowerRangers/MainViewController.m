//
//  MainViewController.m
//  PowerRangers
//
//  Created by Paing Pyi on 14/1/14.
//  Copyright (c) 2014 Paing. All rights reserved.
//

#import "MainViewController.h"

#define PositionChanged @"positionChanged"
#define TouchBegin @"touchBegin"
#define TouchEnd @"touchEnd"

@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bgMapView;

@property (nonatomic, strong) NSArray *powerRangers;
@property (nonatomic, strong) MapView *mapView;
@property (nonatomic, strong) NSMutableArray *powerRangersViews;

@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (nonatomic, strong) UIView *overlay;
@property (nonatomic, strong) NSNumber *positionChanged;
@property CGPoint touchDownPoint;
@property BOOL isAnimating;
@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Navigation
    self.navigationController.navigationBar.translucent = NO;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;   // iOS 7 specific
    
    self.powerRangersViews = [NSMutableArray array];
    
    // UITableView
    [self initTableView];
    
    // Data
    [[PowerRangerManager sharedInstance] generatePowerRangers];
    self.powerRangers = [[PowerRangerManager sharedInstance] getAllPowerRangers];
    
    // MapView
    [self initMapView];
    [self restorePowerRangersOnMap];

    // Overlay
    [self initOverlay];
    
    // Notifications for Event Broadcasting
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveEvent:) name:PositionChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveEvent:) name:TouchBegin object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveEvent:) name:TouchEnd object:nil];

    self.btnSave.enabled = false;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setCustomNavbarRightButtonTitle:(NSString*)title selector:(SEL)selector
{
    if (self.navigationController == nil)
        return;
    if ([title length] <= 0)
        return;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]
                                  initWithTitle:title
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:selector];
    
    [barButton setBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (IBAction)btnSaveClicked:(id)sender {
    [self saveAllCurrentPoints];
    NSLog(@"saved");
}

#pragma mark - TableView

- (void)initTableView
{
    UINib * FinderRetailerCellNib = [UINib nibWithNibName:NSStringFromClass([CustomCell class]) bundle:nil];
    [self.tableView registerNib:FinderRetailerCellNib forCellReuseIdentifier:NSStringFromClass([CustomCell class])];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CustomCell getHeight];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.powerRangers count];
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *CellIdentifier = NSStringFromClass([CustomCell class]);
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    PowerRanger *pr = [self.powerRangers objectAtIndex:indexPath.row];
    [cell configureWithData:pr];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // draw on map
    if(self.isAnimating)
        return;

    self.isAnimating = true;

    PowerRanger *pr = [self.powerRangers objectAtIndex:indexPath.row];
    CustomCell *cell = (CustomCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    PowerRangerView *prv = [self findPRVwithColor:pr.colorValue];

    if(!prv)
        [self showOverlayWithCompletion:^{
            [self deployPowerRangerOnMap:pr];
        }];
    else
        [self deployPowerRangerOnMap:pr];

    
    pr.active = [NSNumber numberWithBool:!pr.activeValue];

    [cell configureWithData:pr];

    // update values
    [self saveCoreData];
    

}

#pragma mark - MapView

- (void)initMapView
{
    self.mapView = [[MapView alloc] initWithFrame:self.bgMapView.frame];
    [self.view addSubview:self.mapView];
}

#pragma mark - PowerRangers Events

- (void)deployPowerRangerOnMap:(PowerRanger*)pr
{
    PowerRangerView *prv;
    CGRect rect;
    CGFloat PRSize = 50;
    
    if([pr.x floatValue] > 0 || [pr.y floatValue] > 0){
        rect = CGRectMake([pr.x floatValue], [pr.y floatValue], PRSize, PRSize);
    }else{
        rect.origin.x = self.mapView.frame.size.width/2 - PRSize/2;
        rect.origin.y = self.mapView.frame.size.height/2 - PRSize/2;
        rect.size.width = PRSize;
        rect.size.height = PRSize;
    }
        
    prv = [self findPRVwithColor:pr.colorValue];

    if(prv){    // already in map so destroy
        
        prv = [self findPRVwithColor:pr.colorValue];
        [self flyUp:prv onCompletion:^{
            [prv removeFromSuperview];
            [self.powerRangersViews removeObject:prv];
            self.isAnimating = false;
        }];
        
        [self savePowerRangersLocation:pr withPoint:CGPointMake(-1, -1)];


    }else{  // add to map
        
        prv = [[PowerRangerView alloc] initWithFrame:rect andObject:pr];
        prv.tag = pr.colorValue;
//        [self putRangersInCenter:prv];

//        [self showOverlayWithCompletion:^{
            [self.mapView addSubview:prv];
            [self flyDown:prv onCompletion:^{

                [self.powerRangersViews addObject:prv];
                self.isAnimating = false;
                [self hideOverlay];
            }];

//        }];

        [self savePowerRangersLocation:pr withPoint:prv.frame.origin];

    }
}


- (void)restorePowerRangersOnMap
{
    float f = 0.1;
    for(PowerRanger *pr in [PowerRanger MR_findAll]){
        if(pr.activeValue){
            [self performSelector:@selector(deployPowerRangerOnMap:) withObject:pr afterDelay:f];
            f += 0.2;
        }
    }
}


#pragma mark - UIHelpers

- (PowerRangerView*)findPRVwithColor:(ColorType)type
{
    PowerRangerView *result = nil;
    for(PowerRangerView *prv in self.powerRangersViews){
        if(prv.tag == type){
            result = prv;
            break;
        }
    }
    return result;
}

- (PowerRanger*)findPRwithTag:(int)tag
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"color = %d",tag];
    PowerRanger *pr = [[self.powerRangers filteredArrayUsingPredicate:predicate] firstObject];
    return pr;
}

- (int)randomBetween:(int)min max:(int)max
{
    int randNum = rand() % (max - min) + min; //create the random number.
    return randNum;
}

#pragma mark - CoreData

- (void)savePowerRangersLocation:(PowerRanger*)pr withPoint:(CGPoint)prv
{
    pr.x = [NSNumber numberWithFloat:prv.x];
    pr.y = [NSNumber numberWithFloat:prv.y];
    [self saveCoreData];
}

- (void)saveCoreData
{
    [[PowerRangerManager sharedInstance] saveToPersistentStoreWithCompletion:nil];
}

#pragma mark - TouchEvents

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.mapView];
    
    if ([touch.view class] == [PowerRangerView class]) {
        PowerRangerView *prv = (PowerRangerView*)touch.view;
        if(!prv)
            return;
        
        [self.mapView bringSubviewToFront:prv];
        
        self.touchDownPoint = touchLocation;
        DLog(@"began : %f,%f",touchLocation.x,touchLocation.y);
        [prv startAnimate];
        
        [self sendEvent:TouchBegin];
    }

}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.mapView];
    
    if ([touch.view class] == [PowerRangerView class]) {
        [self moveViewToPoint:(PowerRangerView*)touch.view withPoint:touchLocation];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([touch.view class] == [PowerRangerView class]) {
        
        PowerRangerView *prv = (PowerRangerView*)touch.view;
        if(!prv)
            return;
        
        [self sendEvent:TouchEnd];
        [prv stopAnimate];
        [self positionChanged:@YES];
    }
}

- (void)moveViewToPoint:(PowerRangerView*)prv withPoint:(CGPoint)location
{
    if(![self isInsideRange:prv withParent:self.mapView]){
        return;
    }
    
    CGPoint originalPoint = prv.frame.origin;
    
    // snap to borders;
    CGFloat newX = originalPoint.x +( location.x - self.touchDownPoint.x);
        newX = newX < 0 ? 0 : newX;
        newX = newX+prv.frame.size.width > self.mapView.frame.size.width ? self.mapView.frame.size.width-prv.frame.size.width : newX;
    
    CGFloat newY = originalPoint.y +( location.y - self.touchDownPoint.y);
        newY = newY < 0 ? 0 : newY;
        newY = newY+prv.frame.size.height > self.mapView.frame.size.height ? self.mapView.frame.size.height-prv.frame.size.height : newY;

    prv.frame = CGRectMake(newX, newY,
    prv.frame.size.width, prv.frame.size.height);
//    [UIView commitAnimations];
    self.touchDownPoint = location;
}

- (BOOL)isInsideRange:(id)childView withParent:(id)parentView
{
    UIView *cv = (UIView*)childView;
    UIView *pv = (UIView*)parentView;
    if(cv.frame.origin.x < 0 || (cv.frame.origin.x + cv.frame.size.width) > pv.frame.size.width){
        return false;
    }
    
    if(cv.frame.origin.y < 0 || (cv.frame.origin.y + cv.frame.size.height) > pv.frame.size.height){
        return false;
    }
    
    return true;
}

- (void)saveAllCurrentPoints
{
    for(PowerRangerView *prv in self.powerRangersViews){
        PowerRanger *pr = [self findPRwithTag:prv.tag];
        if(pr)
            [self savePowerRangersLocation:pr withPoint:prv.frame.origin];
    }
    [self positionChanged:@NO];
}

- (void)updateSaveButtonStatus
{
    self.btnSave.enabled = [self.positionChanged boolValue];
}

#pragma mark - Events

- (void)positionChanged:(NSNumber*)value
{
    self.positionChanged = value;
    [self sendEvent:PositionChanged];
}

- (void)sendEvent:(NSString*)eventName
{
    [[NSNotificationCenter defaultCenter] postNotificationName:eventName
                                                        object:self
                                                      userInfo:nil];
}

- (void)receiveEvent:(NSNotification *)notification {
    
    if([notification.name isEqualToString:PositionChanged]){
        
        [self updateSaveButtonStatus];
    
    }else if([notification.name isEqualToString:TouchBegin]){
    
        
    }else if([notification.name isEqualToString:TouchEnd]){
        
        [self hideOverlay];
        
    }

}

#pragma mark - Animations

- (void)flyUp:(PowerRangerView*)prv
   onCompletion:(void (^)(void))success
{
    CGRect rect = prv.frame;
    CGRect newRect = rect;
    newRect.origin.y = prv.frame.size.height * -1;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         prv.frame = newRect;
                         prv.alpha = 0;
                         
                     } completion:^(BOOL finished) {
                         success();
                     }];}

- (void)flyDown:(PowerRangerView*)prv
        onCompletion:(void (^)(void))success

{
    CGRect rect = prv.frame;
    CGRect newRect = rect;
    newRect.origin.y = prv.frame.size.height * -1;
    prv.frame = newRect;
    prv.alpha = 0;

    CGFloat totalHeightOfPath = rect.origin.y+prv.frame.size.height;
    CGRect rpFrame = CGRectMake(newRect.origin.x, -totalHeightOfPath, newRect.size.width, totalHeightOfPath);
    __block RainbowPath *rp = [[RainbowPath alloc] initWithFrame:rpFrame withColor:prv.backgroundColor];
    rp.alpha = 0;
    [prv.superview addSubview:rp];

    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         prv.frame = rect;
                         prv.alpha = 1;
                         
                         
                     } completion:^(BOOL finished) {
                         success();
                     }];
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         CGRect r = rp.frame;
                         r.origin.y = 0;
                         rp.frame = r;
                         rp.alpha = 0.5;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    [UIView animateWithDuration:0.2
                          delay:0.4
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         rp.transform = CGAffineTransformMakeScale(2, 1);
                         rp.alpha = 0;
                         
                     } completion:^(BOOL finished) {
                         [rp removeFromSuperview];
                     }];
}

- (void)initOverlay
{
    if(self.overlay == nil){
        self.overlay = [[UIView alloc] initWithFrame:self.view.frame];
        [self.view addSubview:self.overlay];
        [self.view bringSubviewToFront:self.mapView];
    }
 
    self.overlay.backgroundColor = [UIColor grayColor];
    self.overlay.hidden = YES;
}

- (void)showOverlayWithCompletion:(void (^)(void))success
{
    __block UIImageView *blur = [[UIImageView alloc] init];

    blur = [self captureBlur];

    [self.overlay addSubview:blur];
    
    self.overlay.alpha = 0;
    self.overlay.hidden = NO;
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                        self.overlay.alpha = 1;
                         
                     } completion:^(BOOL finished) {
                         success();
                     }];
}

- (void)hideOverlay
{

    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         self.overlay.alpha = 0;
                         
                     } completion:^(BOOL finished) {
                         self.overlay.hidden = YES;
                         NSArray *sv = self.overlay.subviews;
                         for(UIView *v in sv)
                             [v removeFromSuperview];
                         
                     }];
}

- (UIImageView*) captureBlur {
    
    //Get a UIImage from the UIView
    NSLog(@"blur capture");
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //Blur the UIImage
    CIImage *imageToBlur = [CIImage imageWithCGImage:viewImage.CGImage];
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName: @"CIGaussianBlur"];
    [gaussianBlurFilter setValue:imageToBlur forKey: @"inputImage"];
    [gaussianBlurFilter setValue:[NSNumber numberWithFloat: 2] forKey: @"inputRadius"]; //change number to increase/decrease blur
    CIImage *resultImage = [gaussianBlurFilter valueForKey: @"outputImage"];
    
    //create UIImage from filtered image
    UIImage *blurrredImage = [[UIImage alloc] initWithCIImage:resultImage];
    
    //Place the UIImage in a UIImageView
    UIImageView *newView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    newView.image = blurrredImage;
    
    return newView;
}

@end

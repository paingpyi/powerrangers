//
//  BaseManager.h
//  TemplateProject
//

#import <Foundation/Foundation.h>

#define SINGLETON_MACRO + (instancetype)sharedInstance { static dispatch_once_t pred; static id __singleton = nil; dispatch_once(&pred, ^{ __singleton = [[self alloc] init]; }); return __singleton; }

@interface BaseManager : NSObject

+ (instancetype)sharedInstance;

+ (NSData*)loadJSONDataFromFileName:(NSString*)filename;
+ (id)loadJSONObjectFromFileName:(NSString*)filename;

@end

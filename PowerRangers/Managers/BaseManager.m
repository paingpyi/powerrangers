//
//  BaseManager.m
//  TemplateProject
//

#import "BaseManager.h"

@interface BaseManager()
@property (nonatomic, strong) NSMutableArray * dataArray;
@end


@implementation BaseManager

+ (instancetype)sharedInstance
{
    DLog(@"WARNING: Must always override +sharedInstance method with SINGLETON_MACRO in subclass of BaseManager");
    
    static dispatch_once_t pred;
    static id __singleton = nil;
	
    dispatch_once(&pred, ^{ __singleton = [[self alloc] init]; });
    return __singleton;
}


#pragma mark - Offline files

+ (NSData*)loadJSONDataFromFileName:(NSString *)filename
{
  if ([filename length] <= 0)
    return nil;
  
  if ([filename hasSuffix:@"json"] == YES)
    filename = [filename stringByReplacingOccurrencesOfString:@".json"
                                                   withString:@""
                                                      options:NSCaseInsensitiveSearch
                                                        range:NSRangeFromString(filename)];
  
  //Read JSON file
  NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:@"json"];
  NSData *fileData = [[NSFileManager defaultManager] contentsAtPath:filePath];
  if (fileData == nil)
    return nil;
  
  return fileData;
}

+ (id)loadJSONObjectFromFileName:(NSString*)filename
{
  NSData *fileData = [BaseManager loadJSONDataFromFileName:filename];
  if (fileData == nil)
    return nil;
  
  //iOS5 JSON Framework
  NSError *error;
  NSDictionary* jsonArray = [NSJSONSerialization JSONObjectWithData:fileData
                                                            options:kNilOptions
                                                              error:&error];
  
  //File contains error
  if (error != nil)
    return error;
  
  return jsonArray;
}




@end

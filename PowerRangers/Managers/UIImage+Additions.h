#import <UIKit/UIKit.h>

@interface UIImage (Additions)

-(UIImage *)scaleToSize:(CGSize)size;
-(UIImage *)scaleToWidth:(CGFloat)width;

-(UIImage *)crop:(CGRect)rect;
-(UIImage *)squareCrop;

-(UIImage *)convertToGrayscale;

/*
 * Generate an image from size & single color
 */
+(UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

- (UIImage *)hueAdjust:(CGFloat)hueAdjust;

- (UIImage *)changeColor:(UIColor*)color;

/*
 * Adjust an image for theme
 * - yellow/blue/red/any string: will be suffix to the base image name
 * - #ABABAB: hex string containing a # character, will be used to colorize the base image
 * - 0-360: a number will be used to perform hue shift on the base image
 */

- (UIColor *)getPixelColorAtLocation:(CGPoint)point;

- (UIColor *)getAverageColor;

@end

//
//  PowerRangerManager.h
//  PowerRangers
//
//  Created by Paing Pyi on 14/1/14.
//  Copyright (c) 2014 Paing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PowerRangerManager : BaseManager

- (NSArray*)getAllPowerRangers;
- (void)generatePowerRangers;
- (NSArray*)colors;
- (void)saveToPersistentStoreWithCompletion:(MRSaveCompletionHandler)completion;

@end

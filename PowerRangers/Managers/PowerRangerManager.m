//
//  PowerRangerManager.m
//  PowerRangers
//
//  Created by Paing Pyi on 14/1/14.
//  Copyright (c) 2014 Paing. All rights reserved.
//

#import "PowerRangerManager.h"

@implementation PowerRangerManager

SINGLETON_MACRO

- (NSArray*)getAllPowerRangers
{
    NSArray *array = [PowerRanger MR_findAll];
    
    return array;
}

- (void)generatePowerRangers
{
    NSArray *array = [self getAllPowerRangers];
    if(array.count > 0)
        return;
    
    NSArray *colors = [self colors];
    
    int i = 0;
    for(NSString *color in colors){
        [self createNewPowerRanger:i name:[colors objectAtIndex:i]];
        i++;
    }
}

- (PowerRanger*)createNewPowerRanger:(ColorType)color name:(NSString*)name
{
    PowerRanger *pr = [PowerRanger MR_createEntity];
    pr.color = [NSNumber numberWithInt:color];
    pr.name = name;
    pr.active = 0;
    [self saveToPersistentStoreWithCompletion:nil];
    
    return pr;
}

- (void)saveToPersistentStoreWithCompletion:(MRSaveCompletionHandler)completion
{
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error){
        if(!success)
            DLog(@"%@", error);
        
        if (completion)
            completion(success, error);
    }];
}


- (NSArray*)colors
{
    return [[NSArray alloc] initWithObjects:@"Red",@"Yellow",@"Green",@"Blue",@"Black",nil];
}

@end

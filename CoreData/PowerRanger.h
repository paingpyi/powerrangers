#import "_PowerRanger.h"

typedef enum {
    Red = 0,
    Yellow = 1,
    Green = 2,
    Blue = 3,
    Black = 4,
} ColorType;

@interface PowerRanger : _PowerRanger {}

- (int)getColor;
- (ColorType)convertStringToColorType;
- (NSString*)getStringOfColorType;
- (UIColor*)getUIColor;

@end

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PowerRanger.m instead.

#import "_PowerRanger.h"

const struct PowerRangerAttributes PowerRangerAttributes = {
	.active = @"active",
	.color = @"color",
	.name = @"name",
	.x = @"x",
	.y = @"y",
};

const struct PowerRangerRelationships PowerRangerRelationships = {
};

const struct PowerRangerFetchedProperties PowerRangerFetchedProperties = {
};

@implementation PowerRangerID
@end

@implementation _PowerRanger

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PowerRanger" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PowerRanger";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PowerRanger" inManagedObjectContext:moc_];
}

- (PowerRangerID*)objectID {
	return (PowerRangerID*)[super objectID];
}

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"activeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"active"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"colorValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"color"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"xValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"x"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}
	if ([key isEqualToString:@"yValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"y"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
	}

	return keyPaths;
}




@dynamic active;



- (BOOL)activeValue {
	NSNumber *result = [self active];
	return [result boolValue];
}

- (void)setActiveValue:(BOOL)value_ {
	[self setActive:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveActiveValue {
	NSNumber *result = [self primitiveActive];
	return [result boolValue];
}

- (void)setPrimitiveActiveValue:(BOOL)value_ {
	[self setPrimitiveActive:[NSNumber numberWithBool:value_]];
}





@dynamic color;



- (int16_t)colorValue {
	NSNumber *result = [self color];
	return [result shortValue];
}

- (void)setColorValue:(int16_t)value_ {
	[self setColor:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveColorValue {
	NSNumber *result = [self primitiveColor];
	return [result shortValue];
}

- (void)setPrimitiveColorValue:(int16_t)value_ {
	[self setPrimitiveColor:[NSNumber numberWithShort:value_]];
}





@dynamic name;






@dynamic x;



- (float)xValue {
	NSNumber *result = [self x];
	return [result floatValue];
}

- (void)setXValue:(float)value_ {
	[self setX:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveXValue {
	NSNumber *result = [self primitiveX];
	return [result floatValue];
}

- (void)setPrimitiveXValue:(float)value_ {
	[self setPrimitiveX:[NSNumber numberWithFloat:value_]];
}





@dynamic y;



- (float)yValue {
	NSNumber *result = [self y];
	return [result floatValue];
}

- (void)setYValue:(float)value_ {
	[self setY:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveYValue {
	NSNumber *result = [self primitiveY];
	return [result floatValue];
}

- (void)setPrimitiveYValue:(float)value_ {
	[self setPrimitiveY:[NSNumber numberWithFloat:value_]];
}










@end

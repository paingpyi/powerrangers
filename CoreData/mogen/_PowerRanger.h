// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PowerRanger.h instead.

#import <CoreData/CoreData.h>


extern const struct PowerRangerAttributes {
	__unsafe_unretained NSString *active;
	__unsafe_unretained NSString *color;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *x;
	__unsafe_unretained NSString *y;
} PowerRangerAttributes;

extern const struct PowerRangerRelationships {
} PowerRangerRelationships;

extern const struct PowerRangerFetchedProperties {
} PowerRangerFetchedProperties;








@interface PowerRangerID : NSManagedObjectID {}
@end

@interface _PowerRanger : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PowerRangerID*)objectID;




@property (nonatomic, strong) NSNumber* active;


@property BOOL activeValue;
- (BOOL)activeValue;
- (void)setActiveValue:(BOOL)value_;

//- (BOOL)validateActive:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* color;


@property int16_t colorValue;
- (int16_t)colorValue;
- (void)setColorValue:(int16_t)value_;

//- (BOOL)validateColor:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSString* name;


//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* x;


@property float xValue;
- (float)xValue;
- (void)setXValue:(float)value_;

//- (BOOL)validateX:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSNumber* y;


@property float yValue;
- (float)yValue;
- (void)setYValue:(float)value_;

//- (BOOL)validateY:(id*)value_ error:(NSError**)error_;






@end

@interface _PowerRanger (CoreDataGeneratedAccessors)

@end

@interface _PowerRanger (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveActive;
- (void)setPrimitiveActive:(NSNumber*)value;

- (BOOL)primitiveActiveValue;
- (void)setPrimitiveActiveValue:(BOOL)value_;




- (NSNumber*)primitiveColor;
- (void)setPrimitiveColor:(NSNumber*)value;

- (int16_t)primitiveColorValue;
- (void)setPrimitiveColorValue:(int16_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitiveX;
- (void)setPrimitiveX:(NSNumber*)value;

- (float)primitiveXValue;
- (void)setPrimitiveXValue:(float)value_;




- (NSNumber*)primitiveY;
- (void)setPrimitiveY:(NSNumber*)value;

- (float)primitiveYValue;
- (void)setPrimitiveYValue:(float)value_;




@end

#import "PowerRanger.h"

@implementation PowerRanger

// Custom logic goes here.

- (ColorType)convertStringToColorType
{
    NSArray *names = [[PowerRangerManager sharedInstance] colors];
    int index = [names indexOfObject:self.name];
    return (ColorType)index;
}

- (NSString*)getStringOfColorType
{
    NSArray *names = [[PowerRangerManager sharedInstance] colors];
    return [names objectAtIndex:[self.color intValue]];
}

- (int)getColor
{
    return [self.color intValue];
}


- (UIColor*)getUIColor
{
    UIColor *color;
    switch (self.colorValue) {
        case Red:
            color = [UIColor redColor];
            break;
        case Green:
            color = [UIColor greenColor];
            break;
        case Blue:
            color = [UIColor blueColor];
            break;
        case Black:
            color = [UIColor blackColor];
            break;
        case Yellow:
            color = [UIColor yellowColor];
            break;
        default:
            color = [UIColor blackColor];
            break;
    }
    return color;
}

@end

